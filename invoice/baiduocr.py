#!/usr/bin python3
# -*- coding:UTF-8 -*-
# Author: nigo
import base64
import requests
import json
import re
from invoice.pdf2img import pyMuPDF_fitz
import os
import pandas as pd

def get_file_path(dictory):
    paths = os.listdir(dictory)
    paths = [os.path.join(dictory,path) for path in paths]
    return paths

def base64_encode(img_path):
    with open(img_path,'rb') as f:
        data = base64.b64encode(f.read())
        img_content = 'base64,%s' % data.decode()
    return img_content

def access_token(client_id,client_secret):
    url = 'https://aip.baidubce.com/oauth/2.0/token'
    params = {
        'grant_type':'client_credentials',
        'client_id':client_id,
        'client_secret':client_secret
    }
    response = requests.post(url,params=params).json()
    return response['access_token']

def clean_format(text):
    """文本信息清洗"""
    return re.sub('\\n','',text)


def extract_info(data):
    """从json数据中提取发票信息
    data:接口返回的json数据
    """
    data = data['words_result']
    info = {
        'invoice_date':data['InvoiceDate'],
        'invoice_type_org':data['InvoiceTypeOrg'],
        'invoice_type':data['InvoiceType'],
        'seller_name':data['SellerName'],
        'seller_register_num':data['SellerRegisterNum'],
        'seller_address':data['SellerAddress'],
        'seller_bank':data['SellerBank'],
        'purchaser_name':data['PurchaserName'],
        'purchaser_register_num':data['PurchaserRegisterNum'],
        'purchaser_address':data['PurchaserAddress'],
        'purchaser_bank':data['PurchaserBank'],
        'amount_in_figuers':data['AmountInFiguers'],
        'total_amount':data['TotalAmount'],
        'total_tax':data['TotalTax'],
        'remarks':data['Remarks'],
        'commodity_name':';'.join([row['word'] for row in data['CommodityName']]),
        'payee':data['Payee'], # 收款人
        'checker':data['Checker'],# 复核人
        'note_drawer':data['NoteDrawer'],#开票人
        'invoice_num':data['InvoiceNum'],# 发票号码
        'invoice_code':data['InvoiceCode'],# 发票代码
    }
    for k,v in info.items():
        info[k] = clean_format(v)
    return info

def baidu_api(token,img_path):
    img_content = base64_encode(img_path)
    url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice'
    params = {
        'access_token':token
    }
    headers = {
        'Content-Type':'application/x-www-form-urlencoded'
    }
    img_content = base64_encode(img_path)
    body = {
        'image':img_content
    }
    response = requests.post(url ,params=params,headers=headers,data=body).json()
    return extract_info(response)

def get_code(text):
    tmp = os.path.splitext(text)[0]
    tmp = tmp.split('/')[-1]
    tmp = tmp.split('_')[0]
    return tmp
