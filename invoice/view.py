from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QTableWidgetItem, QMessageBox, QFileDialog, QComboBox, QFileDialog
from PySide2.QtCore import QDate, Qt, QRegExp
from PySide2.QtGui import QRegExpValidator,QDoubleValidator
from lib.share import resource_path, MySignal
from invoice.baiduocr import *
from threading import Thread
import pandas as pd
import os

# 实例化信号对象
gms = MySignal() # 一个文本信息


class Flag:
    value = True


class WinInvoice:
    """增值税发票识别"""
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/tax_invoice.ui'))
        self.ui.pushButton_path.clicked.connect(self.onPath)
        self.ui.pushButton_start.clicked.connect(self.onStart)
        self.ui.pushButton_restart.clicked.connect(self.ocr)
        self.ui.pushButton_clear.clicked.connect(self.clear)
        self.ui.pushButton_stop.clicked.connect(self.stop)
        self.ui.lineEdit_scale.setValidator(QDoubleValidator(0,2,1))
        self.ui.lineEdit_scale.setText(str(2))
        self.img_dictory = None
        self.token = None
        self.ui.progressBar.setValue(0)
        gms.log.connect(self.log) # 文本框信号
        gms.bar.connect(self.bar) # 更新进度条
        self.process = None

    def log(self, word):
        """更新文本框进度信息"""
        self.ui.textBrowser.append(word)
        self.ui.textBrowser.ensureCursorVisible()

    def bar(self,step):
        self.ui.progressBar.setValue(step)

    def clear(self):
        self.ui.textBrowser.clear()

    def stop(self):
        Flag.value = False

    def onPath(self):
        path = QFileDialog.getExistingDirectory(self.ui,'选择发票所有文件夹','./')
        if path:
            self.ui.lineEdit_path.setText(path)

    def onStart(self):
        pdf_path = self.ui.lineEdit_path.text()
        if os.path.exists(pdf_path):
            self.api_key = self.ui.lineEdit_api_key.text()
            self.secret_key = self.ui.lineEdit_secret_key.text()
            if self.api_key and self.secret_key:
                self.main(pdf_path)
            else:
                QMessageBox.information(self.ui,"提示","请先输入百度接口的key",QMessageBox.Yes,QMessageBox.Yes)
        else:
            QMessageBox.information(self.ui,"提示","请先选择PDF文件夹",QMessageBox.Yes,QMessageBox.Yes)

    def ocr(self):
        count = 0
        step = 0
        Flag.value = True
        if not self.img_dictory:
            pdf_path = self.ui.lineEdit_path.text()
            home_dictory = os.path.dirname(pdf_path)
            self.img_dictory = os.path.join(home_dictory,'img')
            self.csv_path = os.path.join(home_dictory,'invoice.csv')
            self.error_path = os.path.join(home_dictory,'error.txt')
        paths = get_file_path(self.img_dictory)
        if not paths:
            QMessageBox.information(self.ui,"提示","无发票png图片，请点击“开始识别”",QMessageBox.Yes,QMessageBox.Yes)
            return
        if not self.token :
            self.api_key = self.ui.lineEdit_api_key.text()
            self.secret_key = self.ui.lineEdit_secret_key.text()
            self.token = access_token(self.api_key,self.secret_key)
        self.ui.progressBar.setMinimum(0)
        self.ui.progressBar.setMaximum(len(paths))
        for path in paths:
            if not Flag.value: 
                return
            step += 1
            gms.bar.emit(step)
            try:
                data = baidu_api(self.token,path)
                gms.log.emit(str(data))
                df = pd.DataFrame([[v for v in data.values()]],columns=data.keys())
                df['code'] = get_code(path)
                if count == 0:
                    df.to_csv(self.csv_path,mode='a+',index=False)
                else:
                    df.to_csv(self.csv_path,mode='a+',header=False,index=False)
                count += 1
                os.remove(path)
            except:
                gms.log.emit('%s调用失败' % path)
                with open(self.error_path,mode='a+',encoding='utf8') as f:
                    f.write(path + '\n')
        gms.log.emit('=============\n发票转换完成,保存路径为：%s' % self.csv_path)
        gms.log.emit('未识别发票图片路径为:%s' % self.img_dictory)
    

    def main(self,pdf_dictory):
        zoom_x = self.ui.lineEdit_scale.text()
        zoom_y = zoom_x
        def thread_func(paths):
            gms.log.emit('将PDF文档切分为图片')
            for path in paths:
                pyMuPDF_fitz(path,self.img_dictory,zoom_x,zoom_y)
            gms.log.emit('PDF切分图片完成\n=============')
            self.token = access_token(self.api_key,self.secret_key)
            self.ocr()
        home_dictory = os.path.dirname(pdf_dictory)
        self.img_dictory = os.path.join(home_dictory,'img')
        self.csv_path = os.path.join(home_dictory,'invoice.csv')
        self.error_path = os.path.join(home_dictory,'error.txt')
        if os.path.exists(self.csv_path):
            reply = QMessageBox.question(self.ui, '选择',"已有发票数据，是否删除?",QMessageBox.Yes,QMessageBox.No)
            if reply == QMessageBox.Yes:
                os.remove(self.csv_path)
        paths = get_file_path(pdf_dictory)
        thread = Thread(target=thread_func,args=(paths,))
        thread.start()

