from PySide2.QtCore import Signal, QObject
import os
import sys


class SI:
    mainWin = None
    subWinTable = {}


class MySignal(QObject):
    log = Signal(str)
    log3 = Signal(str, str, str)
    bar = Signal(int)


#生成资源文件目录访问路径
def resource_path(relative_path):
    if getattr(sys, 'frozen', False): #是否Bundle Resource
        base_path = sys._MEIPASS
    else:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)
