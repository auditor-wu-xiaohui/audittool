#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# Author: nigo
from PySide2.QtWidgets import QApplication, QMessageBox, QMdiSubWindow
from PySide2.QtUiTools import QUiLoader
from PySide2.QtGui import QPixmap
from lib.share import SI
from PySide2.QtCore import Qt
from lib.share import resource_path
from comformation.view import WinComform
from infomation.view import WinAnn
from invoice.view import WinInvoice


class WinMain:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/main.ui'))
        self.on_info()
        self.ui.actionComformation.triggered.connect(self.on_comform)
        self.ui.actionAnn.triggered.connect(self.on_announcement)
        self.ui.actionOCR.triggered.connect(self.on_invoice)
        self.ui.actionAbout.triggered.connect(self.on_info)
        self.ui.opTree.itemDoubleClicked.connect(self.opTreeAction)
        self.opTreeActionTable = {}

    def on_announcement(self):
        """显示上市公司公告下载信息"""
        self._openSubWin(WinAnn)

    def on_invoice(self):
        """增值税发票识别"""
        self._openSubWin(WinInvoice)

    def on_info(self):
        """显示关于信息"""
        self._openSubWin(WinInfo)

    def on_comform(self):
        """询证函"""
        winComform = self._openSubWin(WinComform)
        winComform.showTree(self.ui)
        self.opTreeActionTable = winComform.opTreeActionTable

    def opTreeAction(self, item, column):
        clickedText = item.text(column)
        if clickedText not in self.opTreeActionTable:
            return

        actionWinFunc = self.opTreeActionTable[clickedText]
        self._openSubWin(actionWinFunc)

    def _openSubWin(self, FuncClass):
        def createSubWin():
            subWinFunc = FuncClass()
            if hasattr(subWinFunc, 'mainWin'):
                # 判断对象有主窗口属性时才传参
                subWinFunc.mainWin = self
            # subWinFunc.showTree()
            subWin = QMdiSubWindow()
            subWin.setWidget(subWinFunc.ui)
            subWin.setAttribute(Qt.WA_DeleteOnClose)
            self.ui.mdiArea.addSubWindow(subWin)
            # 存入表中，注意winFunc对象也要保存，不然对象没有引用，会销毁
            SI.subWinTable[str(FuncClass)] = {
                'subWin': subWin,
                'subWinFunc': subWinFunc
            }
            subWin.show()
            # 子窗口提到最上层，并且最大化
            subWin.setWindowState(Qt.WindowActive | Qt.WindowMaximized)
            return subWinFunc

        # 如果该功能类型 实例不存在
        if str(FuncClass) not in SI.subWinTable:
            #创建实例
            return createSubWin()

        # 如果已经存在，直接show一下
        subWin = SI.subWinTable[str(FuncClass)]['subWin']
        subWinFunc = SI.subWinTable[str(FuncClass)]['subWinFunc']
        try:
            subWin.show()
            # 子窗口提到最上层，并且最大化
            subWin.setWindowState(Qt.WindowActive | Qt.WindowMaximized)
            return subWinFunc
        except:
            # show 异常原因肯定是用户手动关闭了该窗口，subWin对象已经不存在了
            return createSubWin()


class WinInfo:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/info.ui'))
        pixmap = QPixmap(resource_path('ui/qr.jpg'))
        self.ui.label.setPixmap(pixmap)


if __name__ == "__main__":
    app = QApplication([])
    # 实例化主窗口
    SI.main_win = WinMain()
    SI.main_win.ui.show()
    app.exec_()
