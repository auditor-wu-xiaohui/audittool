#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# Author: nigo
import enum
from PySide2.QtWidgets import QApplication, QMessageBox, QMdiSubWindow, QTreeWidgetItem, QFileDialog, QComboBox, QTableWidgetItem
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QStringListModel, Qt
from lib.share import SI, MySignal
import os
import re
from docx import Document
from docxcompose.composer import Composer
from docx.table import _Cell
from docx.oxml import OxmlElement
from docx.oxml.ns import qn
from openpyxl import Workbook, load_workbook
import pandas as pd
from comformation.lib.share import CF
from threading import Thread
import traceback
from lib.share import resource_path

# 实例化信号
gms = MySignal()


# 合局函数，处理表格边框
def set_cell_border(cell: _Cell, **kwargs):
    """
    Set cell`s border
    Usage:
    set_cell_border(
        cell,
        top={"sz": 12, "val": "single", "color": "#FF0000", "space": "0"},
        bottom={"sz": 12, "color": "#00FF00", "val": "single"},
        start={"sz": 24, "val": "dashed", "shadow": "true"},
        end={"sz": 12, "val": "dashed"},
        tr2bl={}
        tl2br={}
    )
    """
    tc = cell._tc
    tcPr = tc.get_or_add_tcPr()

    # check for tag existnace, if none found, then create one
    tcBorders = tcPr.first_child_found_in("w:tcBorders")
    if tcBorders is None:
        tcBorders = OxmlElement('w:tcBorders')
        tcPr.append(tcBorders)

    # list over all available tags
    for edge in ('start', 'top', 'end', 'bottom', 'insideH', 'insideV',
                 'tr2bl', 'tl2br'):
        edge_data = kwargs.get(edge)
        if edge_data:
            tag = 'w:{}'.format(edge)

            # check for tag existnace, if none found, then create one
            element = tcBorders.find(qn(tag))
            if element is None:
                element = OxmlElement(tag)
                tcBorders.append(element)

            # looks like order of attributes is important
            for key in ["sz", "val", "color", "space", "shadow"]:
                if key in edge_data:
                    element.set(qn('w:{}'.format(key)), str(edge_data[key]))


class WinComform:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/com_comformations.ui'))
        self.mainWin = None
        self.ui.pushButton_excel.clicked.connect(self.onSelectExcel)
        self.ui.pushButton_word.clicked.connect(self.onSelectWord)
        self.ui.pushButton.clicked.connect(self.onComfirm)

    def initMainWin(self, mainWin):
        """获取主窗口"""
        self.mainWin = mainWin

    def showTree(self, ui):
        # ui = self.mainWin.ui
        ui.opTree.setVisible(True)
        # 清空树节点
        ui.opTree.clear()
        root = ui.opTree.invisibleRootItem()
        # 创建一个目录节点
        folderItem = QTreeWidgetItem()
        # 设置该节点，第一个column文本
        folderItem.setText(0, '选择数据')
        root.addChild(folderItem) # 添加到root下为第一层节点
        folderItem = QTreeWidgetItem()
        folderItem.setText(0, '非表格信息匹配')
        root.addChild(folderItem)
        folderItem = QTreeWidgetItem()
        folderItem.setText(0, '生成方式')
        root.addChild(folderItem)
        self.opTreeActionTable = {
            '选择数据': WinComform,
            '非表格信息匹配': WinMatch,
            '生成方式': WinMethod,
        }

    def onComfirm(self):
        """点击确认按钮"""
        path_excel, path_word = self.ui.label_excel.text(
        ), self.ui.label_word.text()
        if not os.path.exists(path_excel):
            QMessageBox.warning(self.ui, "警告", "请选择正确的Excel文件路径")
            return
        if not os.path.exists(path_word):
            QMessageBox.warning(self.ui, "警告", "请选择正确的Word文件路径")
            return
        CF.path_excel, CF.path_word = path_excel, path_word
        self.mainWin._openSubWin(WinMatch)

    def onSelectExcel(self):
        reply = QMessageBox.information(self.ui, "提问", "是否已准备好Excel数据模版",
                                        QMessageBox.Yes | QMessageBox.No,
                                        QMessageBox.No)
        if reply == 16384:
            path = QFileDialog.getOpenFileName(self.ui, "打开Excel文件", './',
                                               'xlsx(*.xlsx)')[0]
            if path:
                self.ui.label_excel.setText(path)
                CF.path_excel = path
        else:
            word_file_name = self.ui.label_word.text()
            if os.path.exists(word_file_name) and re.findall(
                    'docx?$', word_file_name):
                self.winTp = WinTemplate()
                self.winTp.ui.show()
            else:
                QMessageBox.information(self.ui, "提示",
                                        "请先选择word模版路径，系统将根据word模版制作Excel模版",
                                        QMessageBox.Yes, QMessageBox.Yes)

    def onSelectWord(self):
        path = QFileDialog.getOpenFileName(self.ui, "打开word文件", './',
                                           'docx(*.docx)')[0]
        if path:
            self.ui.label_word.setText(path)
            CF.path_word = path


class WinTemplate:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/com_template.ui'))
        self.tables_in = []
        self.tables_out = []
        self.get_word_path()
        self.get_table_list()
        self.ui.listView_in.clicked.connect(self.show_table)
        self.ui.listView_out.clicked.connect(self.show_table)
        self.ui.pushButton_add.clicked.connect(self.add_one)
        self.ui.pushButton_add_all.clicked.connect(self.add_all)
        self.ui.pushButton_minus.clicked.connect(self.minus_one)
        self.ui.pushButton_minus_all.clicked.connect(self.minus_all)
        self.ui.pushButton_close.clicked.connect(self.ui.close)
        self.ui.pushButton_next.clicked.connect(self.generate)
        self.list_model_out = QStringListModel()
        self.list_model_out.setStringList([])

    def get_word_path(self):
        self.path = CF.path_word
        self.document = Document(CF.path_word)
        if not os.path.exists(self.path):
            QMessageBox.warning(self.ui, '提示', '请先选择word版本路径', QMessageBox.Yes,
                                QMessageBox.Yes)
            return
        self.document = Document(self.path)
        self.tables_in = [
            '表格' + str(i + 1) for i in range(len(self.document.tables))
        ]

    def sort_table_name(self, tables):
        table_numbers = []
        table_relation = {}
        sorted_table = []
        for table in tables:
            num = table.split('表格')[1]
            table_numbers.append(int(num))
            table_relation[int(num)] = table
        table_numbers.sort()
        for table_number in table_numbers:
            sorted_table.append(table_relation[table_number])
        return sorted_table

    def update_listview(self):
        """刷新两个列表"""
        self.list_model_out.setStringList(self.tables_out)
        self.ui.listView_out.setModel(self.list_model_out)
        self.list_model_in.setStringList(self.tables_in)
        self.ui.listView_in.setModel(self.list_model_in)

    def add_one(self):
        """增加需要输出的表格"""
        row = self.list_model_out.rowCount() # 右列的目前的最大值
        index_out = self.ui.listView_out.currentIndex()
        index_in = self.ui.listView_in.currentIndex()
        if index_in.data() not in self.tables_out and index_in.data():
            self.tables_out.append(index_in.data())
            self.tables_out = self.sort_table_name(self.tables_out)
            self.tables_in.remove(index_in.data())
            self.update_listview()

    def add_all(self):
        """选择所有为需要输出的表格"""
        self.tables_out = self.tables_in + self.tables_out
        self.tables_out = self.sort_table_name(self.tables_out)
        self.tables_in = []
        self.update_listview()

    def minus_one(self):
        """去除需要输出的表格"""
        index_out = self.ui.listView_out.currentIndex()
        index_in = self.ui.listView_in.currentIndex()
        if index_out.data() not in self.tables_in and index_out.data():
            self.tables_in.append(index_out.data())
            self.tables_in = self.sort_table_name(self.tables_in)
            self.tables_out.remove(index_out.data())
            self.update_listview()

    def minus_all(self):
        """去除所有为需要输出的表格"""
        self.tables_in = self.tables_in + self.tables_out
        self.tables_in = self.sort_table_name(self.tables_in)
        self.tables_out = []
        self.update_listview()

    def get_table_list(self):
        self.list_model_in = QStringListModel()
        self.list_model_in.setStringList(self.tables_in)
        self.ui.listView_in.setModel(self.list_model_in)

    def show_table(self, index):
        """展示选中的表格具体表头内容"""
        table_name = index.data()
        k = int(table_name.split('表格')[1]) - 1
        tables = self.document.tables
        column_list = []
        for i in range(len(tables[k].columns)):
            cell_text = tables[k].cell(0, i).text
            column_list.append(cell_text)
        self.ui.tableWidget.setColumnCount(len(column_list))
        self.ui.tableWidget.setRowCount(2)
        self.ui.tableWidget.setHorizontalHeaderLabels(column_list)
        self.ui.tableWidget.setVerticalHeaderLabels(['1', '2'])
        self.ui.tableWidget.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.ui.tableWidget.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

    def word_info(self):
        """提取word中非表格关键字段"""
        paragraphs = self.document.paragraphs
        infos = []
        for paragraph in paragraphs:
            result = re.findall(r'(?<={).*?(?=})', paragraph.text)
            if result:
                infos += result
        return infos

    def generate(self):
        """生成Excel表格"""
        if self.tables_out:
            excel_path = QFileDialog.getSaveFileName(self.ui, '生成模版保存位置',
                                                     './数据模版.xlsx',
                                                     'xlsx(*.xlsx)')[0]
            if excel_path:
                try:
                    wb = Workbook()
                    for table_name in self.tables_out:
                        index = int(table_name.split('表格')[1]) - 1
                        ws = wb.create_sheet(table_name, -1)
                        columns = len(self.document.tables[index].columns)
                        rows = len(self.document.tables[index].rows)
                        for i in range(columns + 1):
                            column = i + 1
                            ws.cell(
                                1,
                                column).value = str(index + 1) + '.' + str(i)
                            if i == 0:
                                ws.cell(2, column).value = 'ID'
                            if i < columns:
                                ws.cell(
                                    2, column + 1
                                ).value = self.document.tables[index].cell(
                                    0, i).text
                    infos = self.word_info()
                    ws = wb.create_sheet('表格0', 0)
                    for index, name in enumerate(infos):
                        ws.cell(1, index + 1).value = '0.' + str(index + 1)
                        ws.cell(2, index + 1).value = name
                    del wb['Sheet']
                    wb.save(excel_path)
                    wb.close()
                    QMessageBox.information(self.ui, '提示', '文件保存成功',
                                            QMessageBox.Yes)
                except:
                    QMessageBox.information(
                        self.ui, '提示', 'Error:' + traceback.format_exc() +
                        '\r\n程序有bug,请反馈至tujiabing81@163.com', QMessageBox.Yes)
        else:
            QMessageBox.information(self.ui, '提示', '请先添加需要的表格来生成模版',
                                    QMessageBox.Yes)


class WinMatch:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/com_match.ui'))
        self.mainWin = None
        self.ui.pushButton.clicked.connect(self.onComfirm)
        self.ui.pushButton_reload.clicked.connect(self.onReload)
        if os.path.exists(CF.path_excel) and os.path.exists(CF.path_word):
            self.path_excel, self.path_word = CF.path_excel, CF.path_word
            self.load_excel()
        else:
            QMessageBox.warning(self.ui, '警告', '请先选择数据', QMessageBox.Yes)
            self.path_excel = ''
            self.path_word = ''
            self.wb = None

    def onReload(self):
        """重新加载数据"""
        self.path_excel, self.path_word = CF.path_excel, CF.path_word
        self.load_excel()

    def onComfirm(self):
        """点击下一步"""
        CF.nontable = self.get_relation()
        self.mainWin._openSubWin(WinMethod)

    def input_path(self, *arg):
        self.path_excel, self.path_word = arg
        self.load_excel()
        # self.tableWidget.cellChanged.connect(self.cellchanged)

    def cellchanged(self):
        for i in range(self.ui.tableWidget.rowCount()):
            table_name = self.ui.tableWidget.cellWidget(i, 0).currentText()
            if table_name != self.table_sheets[i]:
                self.table_sheets[i] = table_name
                ws = self.wb[table_name]
                comBoxCell = QComboBox()
                comBoxCell.addItems(self.get_row_list(ws, 2))
                self.ui.tableWidget.setCellWidget(i, 1, comBoxCell)
                comBoxCell.currentIndexChanged.connect(self.update_code)
        self.update_code()

    def get_relation(self):
        relation = {}
        if self.info:
            for index, value in enumerate(self.info):
                relation[value] = self.ui.tableWidget.item(index, 2).text()
        return relation

    def update_code(self):
        try:
            for i in range(self.ui.tableWidget.rowCount()):
                if self.ui.tableWidget.cellWidget(
                        i, 0) and self.ui.tableWidget.cellWidget(i, 1):
                    table_name = self.ui.tableWidget.cellWidget(
                        i, 0).currentText()
                    name = self.ui.tableWidget.cellWidget(i, 1).currentText()
                    code = self.relation[table_name][name]
                    item = QTableWidgetItem(code)
                    self.ui.tableWidget.setItem(i, 2, item)
        except:
            pass

    def get_row_list(self, ws, row):
        return [ws.cell(row, i).value for i in range(1, ws.max_column + 1)]

    def load_excel(self):
        self.wb = load_workbook(self.path_excel)
        self.relation = {}
        for sheet_name in self.wb.sheetnames:
            ws = self.wb[sheet_name]
            names = self.get_row_list(ws, 2)
            codes = self.get_row_list(ws, 1)
            tmp = {}
            for index, name in enumerate(names):
                tmp[name] = codes[index]
            self.relation[sheet_name] = tmp
        CF.relation = self.relation
        if '表格0' in self.wb.sheetnames:
            ws = self.wb['表格0']
            row1 = self.get_row_list(ws, 1)
            row2 = self.get_row_list(ws, 2)
            self.info = row1
            self.table_sheets = [
                self.wb.sheetnames[0] for i in range(len(row1))
            ]
            self.ui.tableWidget.setColumnCount(3)
            self.ui.tableWidget.setRowCount(len(row1))
            self.ui.tableWidget.setHorizontalHeaderLabels(
                ['取值表', '取值字段', '编号'])
            self.ui.tableWidget.setVerticalHeaderLabels(row2)
            self.ui.tableWidget.setHorizontalScrollBarPolicy(
                Qt.ScrollBarAsNeeded)
            self.ui.tableWidget.setVerticalScrollBarPolicy(
                Qt.ScrollBarAsNeeded)

            for i in range(len(row2)):
                comBoxTable = QComboBox()
                comBoxTable.addItems(self.wb.sheetnames)
                comBoxCell = QComboBox()
                comBoxCell.addItems(
                    self.get_row_list(self.wb[self.wb.sheetnames[0]], 2))
                comBoxCell.currentIndexChanged.connect(self.update_code)
                comBoxCell.setCurrentIndex(i)
                comBoxTable.currentIndexChanged.connect(self.cellchanged)
                self.ui.tableWidget.setCellWidget(i, 1, comBoxCell)
                self.ui.tableWidget.setCellWidget(i, 0, comBoxTable)
                code = self.relation[self.wb.sheetnames[0]][
                    comBoxCell.currentText()]
                item = QTableWidgetItem(code)
                self.ui.tableWidget.setItem(i, 2, item)
                self.ui.tableWidget.update()
        else:
            self.info = None


class WinMethod:
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/com_method.ui'))
        self.ui.comboBox_num.addItems(['合并一个', '单独文件'])
        self.ui.comboBox_blank.addItems(['空白', '无', '左斜线', '右斜线'])
        self.ui.comboBox_table.currentIndexChanged.connect(
            self.combobox_changed)
        self.ui.radioButton.toggled.connect(self.hide)
        self.ui.lineEdit.setHidden(True)
        self.ui.label_2.setHidden(True)
        self.ui.label_3.setHidden(True)
        self.ui.spinBox.setHidden(True)
        self.ui.progressBar.setHidden(True)
        self.ui.pushButton_reload.clicked.connect(self.onReload)
        self.ui.pushButton.clicked.connect(self.onMail)
        self.nontable = CF.nontable
        self.relation = CF.relation
        gms.log.connect(self.log)

        if os.path.exists(CF.path_excel) and os.path.exists(CF.path_word):
            self.wb = load_workbook(CF.path_excel)
            self.doc = Document(CF.path_word)
            self.path_excel = CF.path_excel
            self.path_word = CF.path_word
            self.init()
        else:
            self.wb = None
            self.doc = None
            self.path_excel = None
            self.path_word = None
            QMessageBox.warning(self.ui, '警告', '请先选择数据', QMessageBox.Yes)
        self.relation = CF.relation
        if not self.relation:
            QMessageBox.information(self.ui, '提示', '非表格信息未匹配,请检查',
                                    QMessageBox.Yes)

    def onReload(self):
        """重新加载数据"""
        if os.path.exists(CF.path_excel) and os.path.exists(CF.path_word):
            self.wb = load_workbook(CF.path_excel)
            self.doc = Document(CF.path_word)
            self.path_excel = CF.path_excel
            self.path_word = CF.path_word
            self.nontable = CF.nontable
            self.relation = CF.relation
            self.init()

    def log(self, *texts):
        """信号处理"""
        QMessageBox.information(self.ui, '提示', ''.join(texts), QMessageBox.Yes)

    def onMail(self):
        """检查状态并生成函证"""
        self.method, self.blank, self.group_code = self.get_info()
        if not self.group_code:
            QMessageBox.information(self.ui, '提示', '聚合字段不存在,请检查',
                                    QMessageBox.Yes)
            return
        if not os.path.exists(CF.path_excel) or not os.path.exists(
                CF.path_word):
            QMessageBox.information(self.ui, '提示', 'excel或word文件不存在，请选择数据')
            return
        self.out_dir = QFileDialog.getExistingDirectory(
            self.ui, '生成询证函文件保存目录', '../')
        if self.out_dir:
            self.is_id, self.prefix, self.prefix_num = self.get_prefix()
            thread = Thread(target=self.generate_mail)
            thread.start()

    def get_prefix(self):
        if self.ui.radioButton.isChecked():
            return True, self.ui.lineEdit.text(), self.ui.spinBox.value()
        else:
            return False, None, None

    def hide(self):
        """显示或隐藏编号控件"""
        if self.ui.radioButton.isChecked():
            self.ui.lineEdit.setHidden(False)
            self.ui.label_2.setHidden(False)
            self.ui.label_3.setHidden(False)
            self.ui.spinBox.setHidden(False)
        else:
            self.ui.lineEdit.setHidden(True)
            self.ui.label_2.setHidden(True)
            self.ui.label_3.setHidden(True)
            self.ui.spinBox.setHidden(True)

    def init(self):
        self.ui.comboBox_table.clear()
        self.ui.comboBox_cell.clear()
        self.ui.comboBox_table.addItems(self.wb.sheetnames)
        ws = self.wb[self.wb.sheetnames[0]]
        self.ui.comboBox_cell.addItems(self.get_row_list(ws, 2))

    def get_info(self):
        method = self.ui.comboBox_num.currentText()
        blank = self.ui.comboBox_blank.currentText()
        group_table = self.ui.comboBox_table.currentText()
        group_cell = self.ui.comboBox_cell.currentText()
        try:
            group_code = CF.relation[group_table][group_cell]
            return method, blank, group_code
        except:
            return method, blank, None

    def get_row_list(self, ws, row):
        return [ws.cell(row, i).value for i in range(1, ws.max_column + 1)]

    def combobox_changed(self):
        table_name = self.ui.comboBox_table.currentText()
        if table_name:
            ws = self.wb[table_name]
            self.ui.comboBox_cell.clear()
            self.ui.comboBox_cell.addItems(self.get_row_list(ws, 2))

    def generate_mail(self):
        """批量生成询证函"""
        dfs = []
        self.df0 = pd.DataFrame() # 创建空的dataframe 代表表格0（防止没有表格0时报错）
        for sheet in self.wb.sheetnames:
            # converters = {}
            # for column in self.relation[sheet].values():
            # converters[column] = str
            df = pd.read_excel(self.path_excel, sheet)
            df = df.fillna('')
            df = df.drop(df.index[0])
            if sheet != '表格0':
                df = df.rename(columns={df.columns[0]: 'ID'})
                df = df.set_index(df.columns[0])
                if not df.empty:
                    dfs.append(df)
            else:
                self.df0 = df.reset_index(drop=True) # 表格0

        df_all = dfs[0]
        if self.group_code in df_all.columns:
            for index, df in enumerate(dfs):
                if index != 0:
                    try:
                        df_all = pd.concat([df_all, df], axis=1, sort=False)
                    except:
                        gms.log.emit(self.wb.sheetnames[index] +
                                     ':ID存在重复项，建议Excel模版里将表合并在一张表中,保证ID唯一')
                        return None
            df_all = df_all.fillna('')
            distinct_list = list(set(
                df_all[self.group_code].tolist())) # 获取聚合字段唯一列表
            distinct_list = [i for i in distinct_list if i]
            docs = []
            for index, code in enumerate(distinct_list):
                df = df_all[df_all[self.group_code] == code]
                file_name = str(code) + '.docx'
                try:
                    doc = self.generate_doc(df, index + 1)
                except Exception as e:
                    gms.log.emit('Error:' + traceback.format_exc() +
                                 '\r\n生成产生错误,反馈给tujiabing81@163.com')
                    return
                if self.method == '单独文件':
                    out_path = os.path.join(self.out_dir, file_name)
                    doc.save(out_path)
                else:
                    docs.append(doc)
            if docs:
                try:
                    self.merge_docs(docs)
                except Exception as e:
                    gms.log.emit('Error:' + traceback.format_exc() +
                                 '\r\n生成单独文档试试呢？')
                    return
            gms.log.emit('生成完毕，请查看文件保存位置')
        else:
            gms.log.emit('Error:' + traceback.format_exc() +
                         '您所选择的聚合字段没有数据，请重新选择')

    def generate_doc(self, df, num):
        """生成一封函证"""
        df = df.reset_index(drop=True)
        document = Document(self.path_word)
        i = 0
        for paragraph in document.paragraphs:
            while re.findall(r'\{.+?\}', paragraph.text) and i < 20:
                i += 1
                flag = '0.' + str(i)
                if flag in self.nontable.keys():
                    code = self.nontable[flag]
                else:
                    code = None
                if code != 'id': # 判断是否是聚合字段
                    if code in df.columns:
                        repl = df.loc[0, code]
                    elif code in self.df0.columns and not self.df0.empty:
                        repl = self.df0.loc[0, code]
                    else:
                        continue
                elif code == None:
                    continue
                else:
                    if self.prefix:
                        self.prefix = str(self.prefix)
                    else:
                        self.prefix = ''
                    if self.prefix_num in [0, 1]:
                        repl = self.prefix + str(num)
                    else:
                        if self.prefix_num:
                            repl = self.prefix + str(num).zfill(
                                self.prefix_num)
                        else:
                            repl = ''
                paragraph.text = re.sub(r'\{.*?\}',
                                        repl,
                                        paragraph.text,
                                        count=1)
        i = 0
        style = {"sz": 1, 'color': '000000', 'val': 'single'} # 设置斜线样式
        for table in document.tables:
            i += 1
            flag = str(i) + '.' + '1'
            rows = len(table.rows)
            columns = len(table.columns)
            if flag in df.columns:
                sub_df = df[df[flag] != '']
                sub_df = sub_df.reset_index(drop=True)
                data_num = len(sub_df)
                while len(table.rows) - 1 < data_num:
                    table.add_row()
                for k in range(0, len(table.rows)):
                    for j in range(0, len(table.columns)):
                        if k + 1 <= data_num:
                            key = str(i) + '.' + str(j + 1)
                            if key in df.columns:
                                text = sub_df.loc[k, key]
                                if isinstance(text, (int, float)):
                                    table.cell(k + 1,
                                               j).text = "{:,}".format(text)
                                else:
                                    table.cell(k + 1, j).text = str(text)
            for k in range(0, len(table.rows)):
                for j in range(0, len(table.columns)):
                    if '表格' + str(i) in self.wb.sheetnames:
                        if table.cell(k, j).text == '':
                            if self.blank == '无':
                                table.cell(k, j).text = '无'
                            elif self.blank == '左斜线':
                                set_cell_border(table.cell(k, j), tl2br=style)
                            elif self.blank == '右斜线':
                                set_cell_border(table.cell(k, j), tr2bl=style)
            document.tables[i - 1] = table
        return document

    def insert_row(self, table, ix):
        """指定位置插入行"""
        tbl = table._tbl
        successor = tbl.tr_lst[ix]
        tr = tbl._new_tr()
        for gridCol in tbl.tblGrid.gridCol_lst:
            tc = tr.add_tc()
            tc.width = gridCol.w
        successor.addprevious(tr)
        return table.rows[ix]

    def format_row(self, table):
        """保持添加的最后一行格式和前面一致"""
        columns = len(table.columns)
        for i in range(columns):
            table.cell(-1, i).paragraphs[0].style = table.cell(
                -2, i).paragraphs[0].style

    def merge_docs(self, docs):
        """合并word文档"""
        merged_document = Composer(Document())
        for index, sub_doc in enumerate(docs):
            if sub_doc:
                if index < len(docs) - 1:
                    sub_doc.add_page_break()
                merged_document.append(sub_doc)
        file_name = os.path.join(self.out_dir, '生成文档.docx')
        merged_document.save(file_name)
