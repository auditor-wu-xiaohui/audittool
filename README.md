# audittool
审计瑞士军刀

# 审计瑞士军刀

利用Python库PyQt5编写的审计实用工具。

工具地址：

https://gitee.com/nigo81/audittool/releases

代码也在gitee上公开开源。后续新版本、新功能均在这里公布。

## 万能函证生成器

摆脱Excel邮件合并不够灵活的缺点，以及常见生成品固定模版适用性不广的缺点。

采用根据不同Word自动生成Excel模版，只需要在生成的Excel模版中填写数据，点点按钮就能轻松生成。

操作步骤：

### word模版非表格信息改写

将需要替换的非表格文字用中括号{}包裹起来。

![](https://imgkr2.cn-bj.ufileos.com/aa234f0f-a16c-4226-b9a4-7c18502f275d.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=V%252FBlFLtEocU6ivfMQX7Dcj2JMgM%253D&Expires=1610893478)

### 生成Excel模版

运行软件后，选择word文件。

点击：“选择表格”，选择“No”

![](https://imgkr2.cn-bj.ufileos.com/a212fb61-2e41-46f7-92cb-6b8634ec3130.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=UUJYV9IMaw3Mw%252BlV5hnJTkyN8z8%253D&Expires=1610893624)

点击添加、删除按钮，选择需要生成模版的表格：

![](https://static01.imgkr.com/temp/9737dc7b1f2f4b3fa1be02ca8549678e.png)

点击“生成”


### Excel模版中填写非表格信息

模版中表格几就代表word中第几个表格。

其中表格0比较特殊，代表的是word中非表格中括号的信息。

![](https://static01.imgkr.com/temp/fbbbcbd58b4f4ae9a2f8c31fc65e3339.png)

系统仅会读取表格0中一行数据，也就是说，这里只能填写固定信息，如果是变动信息，就空着，后面生成的时候第一步会让你关联后面表格的字段。

### Excel模版中填写表格信息

需要注意的是账号、日期等设置成文本格式，第一行表头数字也需要设置成文本格式。

2.1,2.2这些列的顺序可以任意改变，也可以把多个表信息写在一张表里。

如：2.1,2.2,....2.11,3.1,3.2...

我们也可以添加字段，比如，非表格信息中有“银行名称”需要变化，我们就添加一列“2.12”，填写在这里。

添加的数据不影响数据生成，因为系统根据表头数据来判断填写word中第几个表格的第几列，而添加的2.12代表word中第二个表格的第12列，而word中没有这列，就不会填写。

还有需要注意的是**ID**的概念，每个表第一列都是ID，系统会根据这个ID值将不同Excel表里的数据关联起来。

所以，这里ID最好是某个唯一字段复制过来，如银行账号。

往来函证里，最好是函证的公司名称等。（注：往来函证最好不要多张表，直接把所有字段填写在一张表里）

![](https://static01.imgkr.com/temp/de2eaea9eb924fcc82c382dd5d18e932.png)

### 生成第一步：匹配非表格信息

对于固定信息都默认就可以，变动信息我们需要关联后面的表。

比如，银行名称和账号是变动的，我需要关联上表格2的对应字段。

如果需要编号，就在最后一列手工填写关键字“id"字样，注意不是勾选前面的下拉框。

![](https://imgkr2.cn-bj.ufileos.com/95a042fb-5cb1-480c-b515-55eacb18b350.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=cOAtSfemwzU%252Bo0yBWBy%252BcbxVCnU%253D&Expires=1610894376)


### 生成第二步：设置生成方式

生成方式：选择合并成一个文件还是每个函证单独一封。

![](https://imgkr2.cn-bj.ufileos.com/0ebceb3b-b719-4b18-8b89-3234ec47f163.png?UCloudPublicKey=TOKEN_8d8b72be-579a-4e83-bfd0-5f6ce1546f13&Signature=HF1VrTEZnNvdP%252BVX9hFi3aSAYFk%253D&Expires=1610894526)

空白外填充：空白，无，左斜线，右斜线

聚合字段，需要你选择某个表里的某个字段，系统会按这个字段聚合，比如我想银行名称相同的为一封函证，那么就用银行名称作为聚合字段，这样同一个银行的账号会聚合在一起。

点击下一步选择文件保存位置就自动生成了。


![](https://static01.imgkr.com/temp/e648115e7cd54de7ae96b876b5e84e3f.png)

注意：设置了数字格式自动是千分位格式，所以非金额的字段如账号，日期等最好设置成文本格式。


