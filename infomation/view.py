from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QTableWidgetItem, QMessageBox, QFileDialog, QComboBox, QFileDialog, QApplication
from PySide2.QtCore import QDate, Qt, QRegExp
from PySide2.QtGui import QRegExpValidator
from lib.share import resource_path, MySignal
from threading import Thread
import traceback
import requests
import pandas as pd
import json
import re
import os

# 实例化信号对象
gms = MySignal() # 一个文本信息


class Flag:
    value = True


class WinAnn:
    """上市公司公告下载"""
    def __init__(self):
        self.ui = QUiLoader().load(resource_path('ui/info_announcement.ui'))
        reg = QRegExp("[0-9]{6}")
        validator = QRegExpValidator(reg, self.ui.lineEdit_code)
        self.ui.lineEdit_code.setValidator(validator)
        self.ui.dateEdit_start.setDate(QDate.currentDate())
        self.ui.dateEdit_end.setDate(QDate.currentDate())
        self.ui.tableWidget.setColumnCount(3)
        self.ui.tableWidget.setRowCount(10)
        self.ui.tableWidget.setHorizontalHeaderLabels(['公司简称', '公告时间', '公告标题'])
        self.ui.tableWidget.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.ui.pushButton_search.clicked.connect(self.on_search)
        self.ui.pushButton_stop.clicked.connect(self.on_stop)
        self.ui.pushButton_export.clicked.connect(self.on_export)
        self.ui.comboBox.addItems(
            ['A股', '港股', '新三板', '老三板', '开放式基金', '封闭式基金', '债券'])
        self.row = 0
        gms.log.connect(self.log1) # 文本框信号
        gms.log3.connect(self.log3) # 表格信息
        self.process = None

    def log1(self, word):
        """更新文本框进度信息"""
        self.ui.textBrowser.append(word)
        self.ui.textBrowser.ensureCursorVisible()

    def log3(self, secname, date, title):
        """更新表格信息"""
        item = QTableWidgetItem(secname)
        self.ui.tableWidget.setItem(self.row, 0, item)
        item = QTableWidgetItem(date)
        self.ui.tableWidget.setItem(self.row, 1, item)
        item = QTableWidgetItem(title)
        self.ui.tableWidget.setItem(self.row, 2, item)
        self.row += 1
        if self.row > 8:
            self.ui.tableWidget.insertRow(self.ui.tableWidget.rowCount() - 1)


    def on_export(self):
        file_path =  QFileDialog.getSaveFileName(self.ui,"保存路径","./公告列表.xlsx" ,"xlsx(*.xlsx)")[0]
        row_count = self.ui.tableWidget.rowCount()
        colum_count = self.ui.tableWidget.columnCount()
        secname = []
        date = []
        title = []
        for i in range(row_count):
            for j in range(colum_count):
                if self.ui.tableWidget.item(i,0):
                    secname.append(self.ui.tableWidget.item(i,0).text())
                else:
                    secname.append('')
                if self.ui.tableWidget.item(i,1):
                    date.append(self.ui.tableWidget.item(i,1).text())
                else:
                    date.append('')
                if self.ui.tableWidget.item(i,2):
                    title.append(self.ui.tableWidget.item(i,2).text())
                else:
                    title.append('')
        data = {
            '公司简称':secname,
            '公告时间':date,
            '公告标题':title,
                    }
        df = pd.DataFrame(data)
        df.to_excel(file_path,index=False)

    def on_stop(self):
        """停止下载"""
        Flag.value = False
        # gms.log.emit('\r\n *** 中断下载 ***')

    def on_search(self):
        """公告下载"""
        Flag.value = True
        code = self.ui.lineEdit_code.text()
        market = self.ui.comboBox.currentText()
        keyword = self.ui.lineEdit_keyword.text()
        start_date = self.ui.dateEdit_start.date().toString('yyyy-MM-dd')
        end_date = self.ui.dateEdit_end.date().toString('yyyy-MM-dd')
        path = QFileDialog.getExistingDirectory(self.ui, '公告文件保存路径', './')
        try:
            self.process = Thread(target=sub_main,
                                  args=(path, code, market, keyword,
                                        start_date, end_date, ''))
            self.process.start()
        except:
            gms.log.emit(traceback.format_exc())


def sub_main(path, code, market, keyword, start_date, end_date, leibie):
    """获取公告及下载主函数"""
    orgid = start(code, market)
    pagenum = 1
    while page(path, code, orgid, pagenum, keyword, start_date, end_date,
               leibie):
        pagenum += 1
    gms.log.emit("\r\n *** 下载结束 ***")


def start(code, market):
    url = 'http://www.cninfo.com.cn/new/information/topSearch/query'
    headers = {
        'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
    }
    data = {"keyWord": code, "maxNum": "11"}
    response = requests.post(url, headers=headers, data=data)
    jsonstr = json.loads(response.text)
    org_dict = {}
    for org in jsonstr:
        org_dict[org["category"]] = org
    if market in org_dict.keys():
        orgid = org_dict[market]["orgId"]
        company_name = org_dict[market]["zwjc"]
        # company_name = re.sub(r'\*', '', company_name)
    else:
        orgid = ''
        # company_name = ''
    return orgid


def page(path, code, orgid, pagenum, keyword, start_date, end_date, leibie):
    url = 'http://www.cninfo.com.cn/new/hisAnnouncement/query'
    headers = {
        "Content-Type":
        "application/x-www-form-urlencoded",
        "Referer":
        "http://www.cninfo.com.cn/new/disclosure/stock?orgId=" + orgid +
        "&stockCode=" + code,
        "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
    }
    data = {
        'pageNum': str(pagenum),
        'pageSize': '30',
        'tabName': 'fulltext',
        'stock': code + ',' + orgid,
        'searchkey': keyword,
        'category': leibie,
        'seDate': start_date + ' ~ ' + end_date,
    }
    response = requests.post(url, headers=headers, data=data)
    jsonstr = json.loads(response.text)
    if not jsonstr['categoryList']:
        for row in jsonstr['announcements']:
            title = row['announcementTitle']
            download_link = "http://www.cninfo.com.cn/new/announcement/download?bulletinId=" + row[
                'announcementId']
            secname = row['secName']
            date = re.findall(r'\d{4}-\d{2}-\d{2}', row['adjunctUrl'])[0]
            pdf_name = code + '+' + date + '+' + title + '.pdf'
            if not Flag.value:
                return None
            download_pdf(download_link, path, pdf_name)
            gms.log.emit('已下载:' + pdf_name)
            gms.log3.emit(secname, date, title)
        if jsonstr['hasMore']:
            return pagenum + 1
        else:
            return None
    else:
        return None


def download_pdf(url, path, pdf_name):
    if os.path.exists(os.path.join(path, pdf_name)) == False:
        if os.path.exists(path) == False:
            os.makedirs(path)
        r = requests.get(url)
        with open(os.path.join(path, pdf_name), 'wb') as f:
            f.write(r.content)


if __name__ == "__main__":
    code = '000002'
    market = 'A股'
    orgid, company_name = start(code, market)
    page('./', code, orgid, 1, '', '2021-01-01', '2021-01-27', '')
